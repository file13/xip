//! I'm a simple Rust program to fetch your external IP address.
//! I find that I need something like this fairly often.
//!
//! By Michael Rossi, liturgyio@protonmail.com
//!
//! This is trivial and public domain code.

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct Xip {
    ip: String,
}


/// I'm a JSON &str that looks like this {"ip": "0.0.0.0"}
pub static EMPTY_IP: &str = r#"{"ip": "0.0.0.0"}"#;

/// I get your external IP from ipify.org.
/// I return a JSON string or else an EMPTY_IP JSON String.
fn get_ip() -> String {
    match reqwest::blocking::get("https://api.ipify.org?format=json") {
        Ok(v) => v.text().unwrap_or(EMPTY_IP.to_string()),
        Err(_) => EMPTY_IP.to_string(),
    }
}

/// I fetch the IP, extract it from the JSON and print it.
fn main() {
    let response = get_ip();
    let xip: Xip = serde_json::from_str(&response)
        .expect(&format!("Unable to parse JSON: {:?}", response));
    println!("{}", xip.ip);
}
