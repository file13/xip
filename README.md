# xip

This is a trivial command line program in Rust to pull and print your external IP.
It pulls from ipify.org.

This seems to be something I need often enough that I decided to create this.

## License
Public Domain
